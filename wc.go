package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type neededHelp struct {
	helpType string
	fileName string
}

func main() {
	var selectedHelp, fileName string
	fmt.Scanf("%s %s", &selectedHelp, &fileName)
	if selectedHelp != "" && fileName != "" {
		needed := neededHelp{helpType: selectedHelp, fileName: fileName}
		doTheNeedful(needed)
	} else {
		printHelp()
	}
}

func doTheNeedful(needed neededHelp) {
	switch needed.helpType {
	case "-c":
		byteCounts := needed.getByteCount()
		fmt.Printf("%s %d \n", needed.fileName, byteCounts)
	case "-m":
		charCount := needed.getCharCount()
		fmt.Printf("%s %d \n", needed.fileName, charCount)
	case "-l":
		newLineCount := needed.getNewLineCount()
		fmt.Printf("%s %d \n", needed.fileName, newLineCount)
	case "-L":
		maxWidth := needed.getMaxLineWidth()
		fmt.Printf("%s %d \n", needed.fileName, maxWidth)
	case "-w":
		wordsCount := needed.getWordsCount()
		fmt.Printf("%s %d \n", needed.fileName, wordsCount)
	case "--help":
		printHelp()
	case "--version":
		fmt.Printf("%s", "1.0.0")
	default:
		printHelp()
	}
}

func (help *neededHelp) getByteCount() int64 {
	file, err := os.Stat(help.fileName)
	if err != nil {
		log.Fatal(err)
	}
	file.Size()
	return file.Size()
}

func (help *neededHelp) getCharCount() int {
	content := getFileContent(help)
	return len(content)
}

func (help *neededHelp) getNewLineCount() int {
	content := getFileContent(help)
	newLineCount := strings.Count(content, "\n")
	return newLineCount
}

func (help *neededHelp) getMaxLineWidth() int {
	file, err := os.Open(help.fileName)
	if err != nil {
		log.Println(err)
	}
	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	maxWidth := 0
	for fileScanner.Scan() {
		if maxWidth < len(fileScanner.Text()) {
			maxWidth = len(fileScanner.Text())
		}
	}
	return maxWidth
}

func (help *neededHelp) getWordsCount() int {
	file, err := os.Open(help.fileName)
	if err != nil {
		log.Println(err)
	}
	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanWords)
	count := 0
	for fileScanner.Scan() {
		count++
	}
	return count
}

func getFileContent(help *neededHelp) string {
	file, err := os.Open(help.fileName)
	var content string
	if err != nil {
		log.Println(err)
	}
	data, err := io.ReadAll(file)
	content = string(data)
	return content
}

func printHelp() {
	fmt.Println(`
		Usage: wc [OPTION]... [FILE]...
		or:  wc [OPTION]... --files0-from=F
		Print newline, word, and byte counts for each FILE, and a total line if
		more than one FILE is specified.  A word is a non-zero-length sequence of
		characters delimited by white space.

		With no FILE, or when FILE is -, read standard input.

		The options below may be used to select which counts are printed, always in
		the following order: newline, word, character, byte, maximum line length.
		-c, --bytes            print the byte counts
		-m, --chars            print the character counts
		-l, --lines            print the newline counts
			--files0-from=F    read input from the files specified by
								NUL-terminated names in file F;
								If F is - then read names from standard input
		-L, --max-line-length  print the maximum display width
		-w, --words            print the word counts
			--help     display this help and exit
			--version  output version information and exit`)
}
